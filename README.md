<h1 align="center">WebSite Monitor</h1>

<p align="center"><img width="120" height="120" src="fastlane/metadata/android/en-US/images/icon.png" alt="logo"/></p>

<p align="center">
Website monitor app helps to monitor/track your Website periodically and notify if any problem in your site.
</p>

<p align="center">
<a href="https://gitlab.com/manimaran/website-monitor/blob/master/LICENSE" alt="GitHub license"><img src="https://img.shields.io/badge/License-GPL%20v3-blue.svg" ></a>
<a href="https://manimaran96.wordpress.com" alt="Developer badge"><img src="https://img.shields.io/badge/developed%20by-Manimaran-blue.svg" ></a>
</p>

<p align="center">
<a href="https://f-droid.org/packages/com.manimarank.websitemonitor/"><img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png" width="200px"></a><a href="https://play.google.com/store/apps/details?id=com.manimarank.websitemonitor"><img src="https://raw.githubusercontent.com/manimaran96/Spell4Wiki/master/files/assets/images/badges/google_play.png" width="200px"></a>
</p>

**Release/Demo App** - [Click here](https://gitlab.com/manimaran/website-monitor/-/releases)


## Features
- App monitor all sites in some period of time interval
- App notify to user if any site down/unknown issue.
- We can configure monitor interval time.
- Monitor Pause/Resume particular site
- Single site quick refresh


## Screen Shot

<center><img src="https://gitlab.com/manimaran/website-monitor/raw/master/files/web_site_monitor_ui.gif" data-canonical-src="https://gitlab.com/manimaran/website-monitor/raw/master/files/web_site_monitor_ui.gif" width="280" height="500" /></center>
<br>


## More Screenshots 

| Main | Menu Options | Pause/Resume | Monitor Intervals |
|:-:|:-:|:-:|:-:|
| ![First](fastlane/metadata/android/en-US/images/phoneScreenshots/1.png?raw=true) | ![Sec](fastlane/metadata/android/en-US/images/phoneScreenshots/2.png?raw=true) | ![Third](fastlane/metadata/android/en-US/images/phoneScreenshots/3.png?raw=true) | ![Fourth](fastlane/metadata/android/en-US/images/phoneScreenshots/4.png?raw=true) |

| Update | Create | Search | Notification |
|:-:|:-:|:-:|:-:|
| ![Fifth](fastlane/metadata/android/en-US/images/phoneScreenshots/5.png?raw=true) | ![Sixth](fastlane/metadata/android/en-US/images/phoneScreenshots/6.png?raw=true) | ![Seventh](fastlane/metadata/android/en-US/images/phoneScreenshots/7.png?raw=true) | ![Eighth](fastlane/metadata/android/en-US/images/phoneScreenshots/8.png?raw=true) |


## Useful links
 * [Blog about Website monitor](https://manimaran96.wordpress.com/category/android-apps/website-monitor/)

## License

<img src="https://raw.githubusercontent.com/manimaran96/Spell4Wiki/master/files/assets/images/badges/gplv3.svg" width="100px"></img>

Website monitor is Free Software: You can use, study share and improve it at your will. 
Specifically you can redistribute and/or modify it under the terms of the [GNU General Public License](https://www.gnu.org/licenses/gpl.html) as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
